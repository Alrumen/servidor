<?php
session_name("tragaperras");
session_start();
$simbolosNumero = 8;  

if (!isset($_SESSION["monedas"]) || !isset($_SESSION["fruta1"])
    || !isset($_SESSION["fruta2"]) || !isset($_SESSION["fruta3"])
    || !isset($_SESSION["puntos"]) || !isset($_SESSION["cara"])) {
    $_SESSION["monedas"] = 0;
    $_SESSION["fruta1"] = rand(1, $simbolosNumero);
    $_SESSION["fruta2"] = rand(1, $simbolosNumero);
    $_SESSION["fruta3"] = rand(1, $simbolosNumero);
    $_SESSION["puntos"] = 0;
    $_SESSION["cara"] = "plain";
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="mclibre-php-ejercicios.css" title="Color">
</head>

<body>

<?php
echo "  <form action=\"tragaperras2.php\" method=\"post\">\n";
echo "    <table style=\"border: black 1px solid; padding: 10px;\">\n";
echo "      <tbody>\n";
echo "        <tr>\n";
echo "          <td style=\"border: black 1px solid; padding: 6px; padding: 10px\">"
    . "<img src=\"img/frutas/$_SESSION[fruta1].svg\" width=\"160\" alt=\"Imagen\"></td>\n";
echo "          <td style=\"border: black 1px solid; padding: 6px; padding: 10px\">"
    . "<img src=\"img/frutas/$_SESSION[fruta2].svg\" width=\"160\" alt=\"Imagen\"></td>\n";
echo "          <td style=\"border: black 1px solid; padding: 6px; padding: 10px\">"
    . "<img src=\"img/frutas/$_SESSION[fruta3].svg\" width=\"160\" alt=\"Imagen\"></td>\n";
echo "          <td style=\"vertical-align: top; text-align: center\">\n";
echo "    </table>\n";
echo "            <p><button type=\"submit\" name=\"accion\" value=\"moneda\">Meter moneda</button></p>\n";
echo "            <p style=\"margin: 0; margin-right: 1210px; font-size: 200%; border: black 1px solid; padding: 6px; padding: 2px\">$_SESSION[monedas]</p>\n";
echo "            <p><button type=\"submit\" name=\"accion\" value=\"jugar\">Jugar</button></p>\n";
if (isset($_SESSION["cara"])) {
    echo "            <p style=\"margin: 1px; margin-right: 1210px; font-size: 200%; border: black 1px solid; padding: 6px; padding: 2px\">";
    
    echo "$_SESSION[puntos]</p>\n";
}
echo "            <p><button type=\"submit\" name=\"accion\" value=\"reiniciar\">Reiniciar juego</button></p>\n";
echo "            <p style=\"margin: 1px; margin-right: 1210px; font-size: 200%; border: black 1px solid; padding: 6px; padding: 2px\">";
    
echo "          </td>\n";
echo "        </tr>\n";
echo "      </tbody>\n";

echo "  </form>\n";
?>
</body>
</html>
