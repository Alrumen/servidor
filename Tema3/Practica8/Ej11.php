<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<style type="text/css">

		body{
			background: #2E4372;
			font-family:   Verdana, sans-serif;
			width: 1000px;
			height: 1000px;
			margin: auto;
			color: rgb( 238, 250, 255) ;
		}

		section{
			border: 8px solid black;
			width: 500px;
			height: 750px;
			margin-top: 20px;
			padding-top: 20px;
			border-radius: 10px;
			background: #697899;
			float: right;
			text-align: center;
		}

		p{
	text-align: center;
}


		h2{
			text-align: center;
			color: white;
		}


		.cajas{
			float: left;
			padding-left: 40px;

		}

		aside {
			float: right;
			width: 300px;
			height: 600px;
			margin-top: 20px;
			margin-left: 30px;
			padding: 15px;				
			border-radius: 10px;
			background: #697899;
			font-family: sans-serif;
			text-align: center;
			color: white;
			font-size: 20px;
			border: 8px solid black;
		}

		button{
    color: #2E4372;
	background: transparent;
	border: 2px solid #2E4372;
	border-radius: 6px;
}
button:hover {
      background-color: #2E4372;
      color: white;
 }

	</style>
</head>
<body>
	<?php
		
		session_start();
			if (isset($_POST['boton1'])) {
				$_SESSION['nombre']=$_POST['nombre'];
		
		}
		
			echo "<h2>Bienvenido/a  ".$_SESSION['nombre']."</h2>";

		if (!isset($_SESSION["productos"])) {
			
		
		$_SESSION["productos"]=array(
			"Televisor" => array(
				"Producto" => "Televisor",
				"Descripcion" => "22 pulgadas",
				"Precio" => 210,
				"Cantidad" => 0
			),

			"Movil" => array(
				"Producto" => "Movil",
				"Descripcion" => "4G",
				"Precio" => 300,
				"Cantidad" => 0
			),

			"MP4" => array(
				"Producto" => "MP4",
				"Descripcion" => "20Gb",
				"Precio" => 13,
				"Cantidad" => 0
				
			),

			"Raton" => array(
				"Producto" => "Raton",
				"Descripcion" => "6000dpi",
				"Precio" => 20,
				"Cantidad" => 0
				
			),

			"Alfombrilla" => array(
				"Producto" => "Alfombrilla",
				"Descripcion" => "Negra",
				"Precio" => 30,
				"Cantidad" => 0
				
			),

			"USB" => array(
				"Producto" => "USB",
				"Descripcion" => "2Gb",
				"Precio" => 5,
				"Cantidad" => 0
				
			)
		);
	}
		
		if (isset($_POST['quitar'])) {
			$_SESSION['productos'][$_POST['quitar']]["Cantidad"]--;
			
		}

		if (isset($_POST['agregar'])) {
			$_SESSION['productos'][$_POST['agregar']]["Cantidad"]++;
			
		}


	?>

		<aside>
			<header>
				<h2>El carrito de la compra</h2>
			</header>
			<?php
			$total=0;
				foreach ($_SESSION["productos"] as $indice => $producto) {
					if ($_SESSION["productos"][$indice]["Cantidad"]>0) {
			echo $_SESSION["productos"][$indice]["Cantidad"]."  ".$_SESSION["productos"][$indice]["Producto"];
				$total = $total+($_SESSION['productos'][$indice]["Cantidad"]*$_SESSION['productos'][$indice]["Precio"]);
			
			?>
			<form action="#" method="POST">
			 <button  type="submit" name="quitar" value='<?php echo $indice ?>'>Quitar</button>
		     </form>
		     <?php
			}
		}
		echo "<strong>Total: ".$total."€</strong>";
		echo "<br/><br/>";
			?>
			<form action="Ej12.php" method="POST">
				<button  type="submit" name="boton"><strong>Confirmar compra</strong></button>
			</form>
		</aside>

		<section>

			<?php
				
				echo'<form action="#" method="POST">';
					$i=0;
    				foreach ($_SESSION["productos"] as $indice => $producto) {
		        		echo '<div class="cajas">';
		        			$i++;
		        ?>
		        	<img src= '<?php echo $i.".jpg" ?>' width=100 height=100>
		        	<br/>

		        <?php
		        	echo '
		                Producto: <strong>'.$_SESSION["productos"][$indice]["Producto"].'</strong><br/><br/>
		                Descripcion: <strong>'.$_SESSION["productos"][$indice]["Descripcion"].'</strong><br/><br/>
		                Precio: <strong>'.$_SESSION["productos"][$indice]["Precio"].'€</strong><br/><br/>
		                <button  type="submit" name="agregar" value="'.$indice.'">Agregar</button>
		            </div>';

		    	}
		    echo '</form>';
		
			?>
		
		</section>
</body>
</html>