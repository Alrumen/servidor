<?php
session_start();


function recoge($var){

    $tmp= (isset($_REQUEST[$var]))
    ? trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "UTF-8"))
    : "";
    return $tmp;
}

$action = recoge("action");
$actionOK = false;


if ($action != "cero" && $action != "subir" && $action != "bajar"){

    header("Location:inicio.php");
    exit;
} else{
    $accionOk = true;
}

if ($accionOk){

    if($action == "cero"){
        $_SESSION["numero"]=0;
    } elseif ($action == "subir"){
        $_SESSION["numero"] ++;
    }  elseif ($action == "bajar"){
        $_SESSION["numero"] --;
    }
    header("Location:inicio.php");
    exit;
}
?>