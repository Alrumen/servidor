<?php
	for ($i = 0; $i < 3; $i++) {
		for ($j = 0; $j < 4; $j++) {
			$arr[$i][$j] = rand(0,10);
		}
	}
	$maxArr = [];
	$arrMedias = [];
	for ($i = 0; $i < 3; $i++) {
		$suma = 0;
		$media = 1;
		array_push($maxArr, max($arr[$i]));
		for ($j = 0; $j < 4; $j++) {
			$suma += $arr[$i][$j];
		}
		$media = $suma / count($arr);
		array_push($arrMedias, $media);
	}
	echo "Maximo de cada fila: ";
	foreach($maxArr as $ma) {
		echo $ma." ";
	}
	echo "<br>Medias de cada fila: ";
	foreach($arrMedias as $am) {
		echo $am." ";
	}
?>
