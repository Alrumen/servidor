<?php
	$numeros = array(
		"tres" => 3,
		"dos" => 2,
		"ocho" => 8,
		"cientoveintitres" => 123,
		"cinco" => 5,
		"uno" => 1
	);
	asort($numeros);
	echo "<table>";
	foreach ($numeros as $indice => $n) {
		echo "<tr>";
		echo "<td>".$indice."</td>";
		echo "<td>".$n."</td>";
		echo "</tr>";
	};
	echo "</table>";
?>
