<?php
	$familia = array(
		$simpson = array(
			"nombre" => "Simpson",
			"padre" => "Homer",
			"madre" => "Marge",
			"hijo1" => "Bart",
			"hijo2" => "Lisa",
			"hijo3" => "Maggie"
		),
		$griffin = array(
			"nombre" => "Griffin",
			"padre" => "Peter",
			"madre" => "Lois",
			"hijo1" => "Chris",
			"hijo2" => "Meg",
			"hijo3" => "Stewie"
		)
	);
	echo "<ul>";
	foreach ($familia as $indice => $f) {
		foreach ($f as $indice2 => $v) {
			if ($v == "Simpson" || $v == "Griffin") {
				echo $v;
			} else {
				echo "<li>".$indice2." ".$v."</li>";
			}
		};
	};
	echo "</ul>";
?>
