<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<?php $hoy = date("Y-m-d");
	session_start();
	if ($_SESSION["usutipo"] == "Asistente") {
		$conectar = mysqli_connect('localhost', "$_SESSION[usutipo]", "$_SESSION[usutipo]", 'consultas');
	?>

		<h1>Asignar Cita</h1>
		<form method="POST" action="#">

			<label>Paciente:</label>
			<select name="paciente">
				<option selected>Seleccione</option>
				<?php
				$pacientes = "SELECT * FROM pacientes";
				$select = mysqli_query($conectar, $pacientes);
				while ($valores = mysqli_fetch_array($select)) {
					echo "<option value=" . $valores["dniPac"] . ">$valores[pacNombres]</option>";
				}
				?>
			</select><br>

			<label>Fecha:</label>
			<input type="date" name="fecha"><br>

			<label>Hora:</label>
			<input type="time" name="hora"><br>

			<label>Medico:</label>
			<select name="medicos">
				<option selected>Seleccione</option>
				<?php
				$medicos = "SELECT * FROM medicos";
				$select = mysqli_query($conectar, $medicos);
				while ($valores = mysqli_fetch_array($select)) {
					echo "<option value=" . $valores["dniMed"] . "=>$valores[medNombres]</option>";
				}
				?>
			</select><br>

			<label>Consultorio:</label>
			<select name="consultorio">
				<option selected>Seleccione</option>
				<?php
				$consultorios = "SELECT * FROM consultorios";
				$select = mysqli_query($conectar, $consultorios);
				while ($valores = mysqli_fetch_array($select)) {
					echo "<option>$valores[idConsultorio]</option>";
				}
				?>
			</select><br>

			<input type="submit" name="enviar" value="Enviar">
		</form>

	<?php
		if (isset($_POST["enviar"])) {
			$insert = "INSERT INTO citas (citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado) VALUES ('$_POST[fecha]', '$_POST[hora]', '$_POST[paciente]', '$_POST[medicos]', $_POST[consultorio], 'Asignado')";
			$select = mysqli_query($conectar, $insert);
			echo "<h3>Cita creada correctamente</h3>";
		}
	} else {
		echo "No tienes permiso";
	}
	?>
</body>

</html>