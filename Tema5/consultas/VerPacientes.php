<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
    session_start();
    $sessionErr = "";

    if ($_SESSION["usutipo"] === "Medico" || $_SESSION["usutipo"] === "Asistente") {
        $enlace = mysqli_connect("localhost", "$_SESSION[usutipo]", "$_SESSION[usutipo]", "consultas");

        if (isset($_POST['cerrarsesion'])) {
            session_destroy();
            header("Location:Login.php");
        }

        $consultaf = mysqli_query($enlace, "SELECT * from pacientes");
        echo '
		<div class="container">
			<div class="consulta">
				<form action="#" method="post">
					<div class="flex space-between">
						<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
					</div>
					<table>
						<thead>
                        <th>Identificacion</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Fecha Nacimiento</th>
                        <th>Sexo</th>
						</thead>
						<tbody>
	';

        while ($fila = mysqli_fetch_array($consultaf)) {
            echo '
							<tr>
								<td> ' . $fila["dniPac"] . '</td>
								<td> ' . $fila["pacNombres"] . '</td>
								<td> ' . $fila["pacApellidos"] . '</td>
                                <td> ' . $fila["pacFechaNacimiento"] . '</td>
								<td> ' . $fila["pacSexo"] . '</td>
							</tr>
		';
        }
        echo '
						</tbody>
					</table>
				</form>
			</div>
		</div>
	';
    } else {
        echo  "No puedes consultar";
    }
    ?>

</body>

</html>