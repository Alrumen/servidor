<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<?php
	session_start();
	if ($_SESSION["usutipo"] == "Asistente" || $_SESSION["usutipo"] == "Administrador") {

		if (isset($_POST["registrarse"])) {

			if ($_POST["password"] != $_POST["password2"]) {
				echo "<h3>Las cotraseñas no coinciden</h3>";
			} else {
				$password = hash_hmac('sha512', $_POST["password"], 'secret');
				$connect = mysqli_connect('localhost', "$_SESSION[usutipo]", "$_SESSION[usutipo]", 'consultas');
				$insert1 = "INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usutipo) VALUES ('$_POST[dni]', '$_POST[ususario]', '$password', 'Activo', 'Paciente')";
				$select = mysqli_query($connect, $insert1);
				$insert2 = "INSERT INTO pacientes (dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo) VALUES ('$_POST[ususario]', '$_POST[nombre]', '$_POST[apellidos]', '$_POST[fecha]', '$_POST[sexo]')";
				$select = mysqli_query($connect, $insert2);
				echo "<h3>Cuenta creada correctamente</h3>";
			}
		}
	?>

		<h3>Registrar Paciente</h3>
		<form action="#" method="post">

			<label> Identificacion: </label>
			<input type="text" name="dni"> <br>

			<label> Nombre: </label>
			<input type="text" name="nombre"><br>

			<label> Apellidos: </label>
			<input type="text" name="apellidos"><br>

			<label> Fecha Nacimiento: </label>
			<input type="date" id="start" name="fecha"><br>

			<label> Sexo: </label>
			<select name="sexo">
				<option value="Masculino">Masculino</option>
				<option value="Femenino">Femenino</option>
			</select><br>

			<label> Usuario: </label>
			<input type="text" name="ususario"><br>

			<label> Contraseña: </label>
			<input type="password" name="password">
			<br>

			<label> Repetir Contraseña: </label>
			<input c type="password" name="password2"><br>

			<button type="submit" name="registrarse">Registrar</button>
			<br>

		</form>

	<?php
	} else {
		echo "No tienes permiso";
	}

	?>
</body>

</html>