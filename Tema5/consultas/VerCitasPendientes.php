<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
    session_start();
    $sessionErr = "";

    if ($_SESSION["usutipo"] === "Medico" || $_SESSION["usutipo"] === "Asistente") {
        $enlace = mysqli_connect("localhost", "$_SESSION[usutipo]", "$_SESSION[usutipo]", "consultas");

        if (isset($_POST['cerrarsesion'])) {
            session_destroy();
            header("Location:Login.php");
        }

        $consultaf = mysqli_query($enlace, "SELECT * from citas, medicos WHERE citMedico= dniMed AND citEstado='Asignado'");
        echo '
		<div class="container">
			<div class="consulta">
				<form action="#" method="post">
					<div class="flex space-between">
						<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
					</div>
					<table>
						<thead>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Paciente</th>
                        <th>Médico</th>
                        <th>Consultorio</th>
                        <th>Estado</th>
                        <th>Observaciones</th>
						</thead>
						<tbody>
	';

        while ($fila = mysqli_fetch_array($consultaf)) {
            echo '
							<tr>
								<td> ' . $fila["citFecha"] . '</td>
								<td> ' . $fila["citHora"] . '</td>
								<td> ' . $fila["citPaciente"] . '</td>
                                <td> ' . $fila["medNombres"] . ' ' . $fila["medApellidos"] . '</td>
                                <td> ' . $fila["citConsultorio"] . '</td>
								<td> ' . $fila["citEstado"] . '</td>
                                <td> ' . $fila["CitObservaciones"] . ' </td>
							</tr>
		';
        }
        echo '
						</tbody>
					</table>
				</form>
			</div>
		</div>
	';
    } else {
        echo  "No puedes consultar";
    }
    ?>

</body>

</html>