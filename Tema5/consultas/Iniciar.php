<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>

	<?php
	session_start();
	if (isset($_POST["cerrar"])) {
		session_destroy();
		echo "<br><span>El Usuario ha cerrado sesion<span>";
	}

	if (!isset($_POST["enviar"])) {
	?>

		<h1>Iniciar sesión</h1>
		<form action="#" method="POST">
			<label>Login: </label>
			<input type="text" name="login" required><br><br>

			<label>Contraseña: </label>
			<input type="password" name="password" required><br><br>

			<input type="submit" value="Iniciar sesión" name="enviar">
		</form>
		<?php
	}

	if (isset($_POST["enviar"])) {
		$_SESSION["usutipo"] = "";
		$password = hash_hmac('sha512', $_POST["password"], 'secret');
		$conectar = mysqli_connect('localhost', 'Paciente', 'Paciente', 'consultas');
		$usuario = "SELECT dniUsu,usuLogin,usuPassword,usutipo from usuarios
			where usuLogin='$_POST[login]' and usuPassword='$password'";
		$select = mysqli_query($conectar, $usuario);
		$fila = mysqli_fetch_assoc($select);

		if (!empty($fila)) {
			$_SESSION["usutipo"] = $fila["usutipo"];
			$_SESSION["idusuario"] = $fila["dniUsu"];
		}

		if (empty($_SESSION["usutipo"])) {
			echo "<br/>Usuario: $_POST[login]<br/>Contraseña: $password<br/>El usuario intrducido no existe";
		}

		if ($_SESSION["usutipo"] == "Administrador") {
		?>
			<h1>Administrador</h1>
			<form method="POST" action="AltaPaciente.php">
				<input type="submit" value="Alta Paciente">
			</form><br>

			<form method="POST" action="AltaMedico.php">
				<input type="submit" value="Alta Medico">
			</form><br>

			<form method="POST" action="#">
				<input type="submit" name="cerrar" value="Cerrar sesion">
			</form><br>

		<?php
		}

		if ($_SESSION["usutipo"] == "Medico") {
		?>
			<h1>Médico</h1>
			<form method="POST" action="VerCitasAtendidas.php">
				<input type="submit" value="Ver Citas Atendidas">
			</form><br>

			<form method="POST" action="VerCitasPendientes.php">
				<input type="submit" value="Ver Citas Pendientes">
			</form><br>

			<form method="POST" action="VerPacientes.php">
				<input type="submit" value="Ver Pacientes">
			</form><br>

			<form method="POST" action="#">
				<input type="submit" name="cerrar" value="Cerrar sesion">
			</form><br>

		<?php
		}

		if ($_SESSION["usutipo"] == "Asistente") {
		?>
			<h1>Asistente</h1>
			<form method="POST" action="VerCitasAtendidas.php">
				<input type="submit" value="Ver Citas Atendidas">
			</form><br>

			<form method="POST" action="NuevaCita.php">
				<input type="submit" value="Nueva Cita">
			</form><br>

			<form method="POST" action="AltaPaciente.php">
				<input type="submit" value="Alta Paciente">
			</form><br>

			<form method="POST" action="VerPacientes.php">
				<input type="submit" value="Ver Pacientes">
			</form><br>

			<form method="POST" action="#">
				<input type="submit" name="cerrar" value="Cerrar sesion">
			</form><br>

		<?php
		}

		if ($_SESSION["usutipo"] == "Paciente") {
		?>
			<h1>Paciente</h1>
			<form method="POST" action="VerCitas.php">
				<input type="submit" value="Ver Citas">
			</form><br>

			<form method="POST" action="#">
				<input type="submit" name="cerrar" value="Cerrar sesion">
			</form><br>
	<?php

		}
	}
	?>

</body>

</html>