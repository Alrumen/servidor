<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<?php
		session_start();
		$conexion=mysqli_connect("localhost","root","","ventas");
		$articulo=mysqli_query($conexion,"SELECT * FROM articulos");
		$consulta=mysqli_fetch_array($articulo);
		$_SESSION['articulos'] = $consulta;
		if (!isset($_SESSION['carrito'])) {
			$_SESSION['carrito']=array();
		}

		if (isset($_POST['agregar'])) {
			$key = array_search($_POST['agregar'], array_column($_SESSION['carrito'], 0));
			if ($key!==false) {
				$_SESSION['carrito'][$key][3]++;
				
			}
			else {
				array_push($_SESSION['carrito'],[$_POST['agregar'], $_POST['Descripcion'], $_POST['Precio'], 1]);
			}

		}

		else if (isset($_POST['quitar'])) {
			$clave = array_search($_POST['quitar'], array_column($_SESSION['carrito'], 0));
			if ($clave!==false) {
				$_SESSION['carrito'][$clave][3]--;
			}

			if ($_SESSION['carrito'][$clave][3]==0) {
				unset($_SESSION['carrito'][$clave]);
				array_values($_SESSION['carrito']);
			}
		}


		else if (isset($_POST['guardar'])) {
			$fecha=date('Y-m-d H:i:s');
			for ($i=0; $i < count($_SESSION['carrito']); $i++) { 
			$_SESSION['compra'] = mysqli_query($conexion,"INSERT INTO compras (idusuario,Id_articulo,fecha,cantidad,Precio)
				VALUES ('{$_SESSION['idusuario']}', '{$_SESSION['carrito'][$i][0]}', '{$fecha}', '{$_SESSION['carrito'][$i][3]}', '{$_SESSION['carrito'][$i][2]}')");
			}
			echo "<p>La compra realizada se almacenó correctamente en la base de datos Ventas</p>";
		}


		echo "<h2>Carrito</h2>";
		echo "<div class='cajas1'>";
			echo "<table>";
			echo "<tr>";
			echo "<th>Id_articulo</th>";
			echo "<th>Descripcion</th>";
			echo "<th>Precio</th>";
			echo "<th>Caracteristicas</th>";
			echo "<th>Agregar</th>";
			echo "</tr>";
			while ($_SESSION['articulos'] = mysqli_fetch_array($articulo)) {
				echo "<tr>";
				echo "<td>".$_SESSION['articulos']['Id_articulo']."</td>";
				echo "<td>".$_SESSION['articulos']['Descripcion']."</td>";
				echo "<td>".$_SESSION['articulos']['Precio']."€</td>";
				echo "<td>".$_SESSION['articulos']['Caracteristicas']."</td>";
				echo "<td><form action='#' method='POST'><input type='hidden' name='Descripcion' value='".$_SESSION['articulos']['Descripcion']."'><input type='hidden' name='Precio' value='".$_SESSION['articulos']['Precio']."'><button type='submit' name='agregar' value='".$_SESSION['articulos']['Id_articulo']."'>Agregar</button></form></td>";
				echo "</tr>";
			}
			echo "</table>";
		echo "</div>";

		echo "<div class='cajas2'>";
			echo "<header>";
				echo "<h2>El carrito de la compra</h2>";
			echo "</header>";
				$Precio=0;
				$total=0;
				for ($i=0; $i < count($_SESSION['carrito']); $i++) { 
					if ($_SESSION['carrito'][$i][3]>0) {
						echo $_SESSION['carrito'][$i][3]." ".$_SESSION['carrito'][$i][1]." ";
						$Precio=$_SESSION['carrito'][$i][3]*$_SESSION['carrito'][$i][2];
						echo $Precio."€";
						echo "<form action='#' method='POST'>";
						echo "<button type='submit' name='quitar' value='".$_SESSION['carrito'][$i][0]."'>Quitar</button>";
						echo "</form>";
					}
					$total = $total+$Precio;
				}
			
			echo "<strong>Total: ".$total."€</strong>";
			echo "<br/><br/>";
		
			echo "<form action='#' method='POST'>";
				echo "<button type='submit' name='guardar'><strong>Guardar compra</strong></button>";
			echo "</form>";
			echo "<br/>";
			echo "<a href='Inicio.php'>Inicio</a>";
		echo "</div>";
		mysqli_close($conexion);
		echo "<br/>";
		?>
</body>
</html>