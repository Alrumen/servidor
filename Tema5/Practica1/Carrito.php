<!DOCTYPE html>
<html lang="en">

<head>
    <title>Carrito</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
    <?php
    $link = new PDO('mysql:host=localhost;dbname=ventas', 'root', '');
    ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>DESCRIPCION</th>
                <th>PRECIO</th>
                <th>CARACTERISTICAS</th>
            </tr>
        </thead>
        <?php foreach ($link->query('SELECT * from articulos') as $row) { ?>
            <tr>
                <td><?php echo $row['Id_articulo'] ?></td>
                <td><?php echo $row['Descripcion'] ?></td>
                <td><?php echo $row['Precio'] ?></td>
                <td><?php echo $row['Caracteristicas'] ?></td>
                <td>
                    <form action="#" method="POST">
                        <button type="submit" name="agregar" value="'.$row.'">Agregar</button>
                    </form>
                </td>
            </tr>
        <?php
        }

        foreach ($link->query('SELECT * from usuarios') as $row2) {
            $row2['idusuario'];
        }

        if (!isset($_SESSION["compras"])) {
            $_SESSION["compras"] = array();
        }

        if (isset($_POST['quitar'])) {
            $_SESSION['compras'][$_POST['quitar']][3]--;
        }


        if (isset($_POST['agregar'])) {
            $key = array_search($_POST['agregar'], array_column($_SESSION['compras'], 0));
            if ($key!==false) {
            $_SESSION['compras'][$key][3]++;
            }
            else{
            array_push($_SESSION["compras"], $row2['idusuario'], $row['Id_articulo'], "Fecha", 1, $row['Precio']);
            }
            var_dump($_SESSION["compras"]);
        }



        ?>

        <aside>
            <header>
                <h2>El carrito de la compra</h2>
            </header>

            <form action="#" method="POST">
                <button type="submit" name="quitar" value='<?php echo $row ?>'>Quitar</button>
            </form>
            <?php


            echo "<br/><br/>";
            ?>
            <form action="#" method="POST">
                <button type="submit" name="boton"><strong>Confirmar compra</strong></button>
            </form>
        </aside>

        <section>

            <?php

            echo '<form action="#" method="POST">';
            $i = 0;
            foreach ($_SESSION["compras"] as $indice => $iarticulo) {
                echo '<div class="cajas">';
                $i++;
            }
            ?>

                <br />

            <?php
                if ($_SESSION["compras"][3] > 0) {
                    echo '
                    id-articulo: <strong>' . $row['Id_articulo'] . '</strong><br/><br/>
                    Cantidad: <strong>' . $_SESSION["compras"][3] . '</strong><br/><br/>
                    Precio: <strong>' . $row['Precio'] . '€</strong><br/><br/>
                   
                </div>';
                }
                echo '</form>';
            
            ?>

        </section>

    </table>
</body>

</html>