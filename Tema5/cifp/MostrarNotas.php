<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>
<?php
        if (!isset($_POST["enviar"])) {

    ?>
    <form class="formulario" action="#" method="post">

        <input class="form-input" type="text" name="asignatura" placeholder="Asignatura " required>


        <button type="submit" name="enviar">Iniciar Sesion</button>

    </form>
	<?php
		}
	session_start();

	$sessionErr = "";

	if ($_SESSION["rol"] === "director") {
		$_SESSION["connection"] = mysqli_connect("localhost", "director", "director", "cifp");
	}
	else {
		$sessionErr = "No puedes consultar";
	}
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	if (isset($_POST["enviar"])) {

	echo '
		<div class="container">
			<div class="consulta">
				<form action="#" method="post">
					<div class="flex space-between">
						<p>Conectado el usuario ' . $_SESSION["idusuario"] . ' con el rol ' . $_SESSION["rol"] . '</p>
						<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
					</div>
					<p class="error">' . $sessionErr . '</p>
					<table>
						<thead>
							<th>ID</th>
							<th>Descripcion</th>
							<th>Precio</th>
						
						</thead>
						<tbody>
	';

		if ($_POST['asignatura'] == "Lengua") {
			$consulta = 'SELECT * FROM `notas` WHERE asignatura = "Lengua"';
			$consultaArticulo = mysqli_query($_SESSION["connection"], $consulta);
			while ($fila = mysqli_fetch_array($consultaArticulo)) {
		echo '
							<tr>
								<td> ' . $fila["alumno"] . '</td>
								<td> ' . $fila["fecha"] . '</td>
								<td> ' . $fila["nota"] . ' </td>
							</tr>
		';}
		}else if ($_POST['asignatura'] == "Matemáticas") {
			$consulta = 'SELECT * FROM `notas` WHERE asignatura = "Matemáticas"';
			$consultaArticulo = mysqli_query($_SESSION["connection"], $consulta);
			while ($fila = mysqli_fetch_array($consultaArticulo)) {
			echo '
			<tr>
				<td> ' . $fila["alumno"] . '</td>
				<td> ' . $fila["fecha"] . '</td>
				<td> ' . $fila["nota"] . ' </td>
			</tr>
';
		}
	}
	echo '
						</tbody>
					</table>
				</form>
			</div>
		</div>
	';

	$sql = mysqli_query($_SESSION["connection"], 'SELECT ROUND(AVG(nota),2), MIN(nota), MAX(nota), COUNT(alumno) FROM notas WHERE asignatura="'.$_POST['asignatura'].'"');
	while ($datos=mysqli_fetch_array($sql)){
	echo 'La nota media es ' . $datos["ROUND(AVG(nota),2)"]. '<br>';
	echo 'La nota mas alta es ' . $datos["MAX(nota)"]. '<br>';
	echo 'La nota mas baja es ' . $datos["MIN(nota)"]. '<br>';
	echo 'La nota media es ' . $datos["COUNT(alumno)"]. '';

	}
}
	?>
</body>
