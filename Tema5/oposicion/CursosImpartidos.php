<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<?php
	session_start();
	$sessionErr = "";

	if ($_SESSION["rol"] === "profesor") {
		$_SESSION["connection"] = mysqli_connect("localhost", "Profesor", "Profesor", "oposicion");
	}
	else {
		$sessionErr = "No puedes consultar";
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:Login.php");
	}
	
	$consultaf = mysqli_query($_SESSION["connection"], 'SELECT curso.codigocurso, curso.nombrecurso, curso.maxalumnos, curso.fechaini, curso.fechafin, curso.numhoras, curso.profesor FROM curso,profesor WHERE curso.profesor=profesor.dniP AND profesor.dniP="'.$_SESSION['dniProfe'].'"');
	echo '
		<div class="container">
			<div class="consulta">
				<form action="#" method="post">
					<div class="flex space-between">
						<p>Conectado el usuario ' . $_SESSION["dniProfe"] . ' Nombre: ' . $_SESSION["profesor"] . '</p>
						<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
					</div>
					<p class="error">' . $sessionErr . '</p>
					<table>
						<thead>
							<th>CodigoCurso</th>
							<th>Nombre</th>
							<th>MAX</th>
							<th>FechaINI</th>
							<th>FechaFin</th>
                            <th>NumHoras</th>
                            <th>Profesor</th>
						</thead>
						<tbody>
	';

	while ($fila = mysqli_fetch_array($consultaf)) {
		echo '
							<tr>
								<td> ' . $fila["codigocurso"] . '</td>
								<td> ' . $fila["nombrecurso"] . '</td>
								<td> ' . $fila["maxalumnos"] . ' </td>
                                <td> ' . $fila["fechaini"] . '</td>
								<td> ' . $fila["fechafin"] . '</td>
								<td> ' . $fila["numhoras"] . ' </td>
                                <td> ' . $fila["profesor"] . ' </td>
							</tr>
		';
	}
	echo '
						</tbody>
					</table>
				</form>
			</div>
		</div>
	';
    $sql = mysqli_query($_SESSION["connection"], 'SELECT SUM(numhoras) FROM curso WHERE profesor="'.$_SESSION['dniProfe'].'"');
	while ($datos=mysqli_fetch_array($sql)){
	echo 'Total de horas impartidas ' . $datos["SUM(numhoras)"]. '<br>';

    }
	?>
</body>